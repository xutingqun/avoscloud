package com.example.leandb;

import java.util.ArrayList;
import java.util.List;

import com.alibaba.fastjson.JSON;
import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVObject;
import com.avos.avoscloud.AVQuery;
import com.avos.avoscloud.FindCallback;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		final TextView tvHello=(TextView) findViewById(R.id.tv_hello);
		findViewById(R.id.btn_click).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				AVQuery<AVObject> query = new AVQuery<AVObject>("category");
				query.findInBackground(new FindCallback<AVObject>() {
					
					@Override
					public void done(List<AVObject> arg0, AVException arg1) {
						ArrayList<Category> categoryList=new ArrayList<Category>();
						for (AVObject object : arg0) {
							Category category=(Category) JSON.parse(object.toJSONObject().toString());
							categoryList.add(category);
						}
						
						StringBuffer sb=new StringBuffer();
						for (Category category : categoryList) {
							sb.append(category.name+" , ");
						}
						tvHello.setText(sb.toString());
						
					}
				});
				
			}
		});
	}
}
