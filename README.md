#avoscloud
https://leancloud.cn/docs/start.html

门引导

下载 Android SDK

下载 SDK，将下载后的文件解压缩后的所有 jar 文件放入 Android 项目的 libs 目录。如果你们的项目没有 libs 目录，那么就在项目的根目录下创建一个，通过右键点击项目 Project，选择 New，接下来点击 Folder 菜单即可创建新目录。

添加下列 import 语句到你的 Application 或主 Activity 类：

import com.avos.avoscloud.AVOSCloud;
import com.avos.avoscloud.AVAnalytics;
在 Application 的 onCreate 方法调用 AVOSCloud.initialize 来设置您应用的 Application ID 和 Key：

public void onCreate() {
    //如果使用美国节点，请加上这行代码 AVOSCloud.useAVCloudUS();
    AVOSCloud.initialize(this, "{{appid}}", "{{appkey}}");
}
 void onCreate() {
    //如果使用美国节点，请加上这行代码 AVOSCloud.useAVCloudUS();
    AVOSCloud.initialize(this, "{{appid}}", "{{appkey}}");
}
创建应用后，可以在 控制台 - 应用设置 里面找到应用对应的 id 和 key。

同时，你的应用需要请求 INTERNET 和 ACCESS_NETWORK_STATE 权限，如果没有设置，请添加下列两行到你的 AndroidManifest.xml 文件里的 <application> 标签前：

<uses-permission android:name="android.permission.INTERNET" />
<uses-permission android:name="android.permission.ACCESS_NETWORK_STATE" />
如果你想跟踪统计应用的打开情况，添加下列代码到你的主 Activity 的 onCreate 方法：

AVAnalytics.trackAppOpened(getIntent());
接下来可以尝试测试一段代码，拷贝下列代码到你的 app 里，比如放到 Application.onCreate 方法：

AVObject testObject = new AVObject("TestObject");
testObject.put("foo", "bar");
testObject.saveInBackground();
运行你的 app。一个类 TestObject 的新对象将被发送到 LeanCloud 并保存下来。当你做完这一切，访问 控制台 - 数据管理 可以看到上面创建的 TestObject 的相关数据。


https://leancloud.cn/docs/android_guide.html#基本查询

基本查询

在许多情况下，getInBackground 是不能检索到符合你的要求的数据对象的。AVQuery 提供了不同的方法来查询不同条件的数据。 使用 AVQuery 时，先创建一个 AVQuery 对象，然后添加不同的条件，使用 findInBackground 方法结合FindCallback 回调类来查询与条件匹配的 AVObject 数据。例如，查询指定人员的信息，使用 whereEqualTo 方法来添加条件值：

AVQuery<AVObject> query = new AVQuery<AVObject>("GameScore");
query.whereEqualTo("playerName", "steve");
query.findInBackground(new FindCallback<AVObject>() {
    public void done(List<AVObject> avObjects, AVException e) {
        if (e == null) {
            Log.d("成功", "查询到" + avObjects.size() + " 条符合条件的数据");
        } else {
            Log.d("失败", "查询错误: " + e.getMessage());
        }
    }
});
findInBackground 方法是在后台线程中执行查询数据操作，它和 getInBackground 的运行方式是一样的。如果你已经运行在一个后台上，那么你可以在你的后台线程中直接使用 query.find() 方法来获取数据：

// 如果你的代码已经运行在一个后台线程，或只是用于测试的目的，可以使用如下方式。
AVQuery<AVObject> query = new AVQuery<AVObject>("GameScore");
query.whereEqualTo("playerName", "steve");
try {
    List<AVObject> avObjects = query.find();
} catch (AVException e) {
    Log.d("失败", "查询错误: " + e.getMessage());
}
查询条件

如果要过滤掉特定键的值时可以使用 whereNotEqualTo 方法。比如需要查询 playerName 不等于“steve”的数据时可以这样写：

query.whereNotEqualTo("playerName", "steve");
当然，你可以在你的查询操作中添加多个约束条件（这些条件是 and 关系），来查询符合你要求的数据。

query.whereNotEqualTo("playerName", "steve");
query.whereGreaterThan("age", 18);
有些时候，在数据比较多的情况下，你希望只查询符合要求的多少条数据即可，这时可以使用 setLimit 方法来限制查询结果的数据条数。默认情况下 Limit 的值为 100，最大 1000，在 0 到 1000 范围之外的都强制转成默认的 100。

query.setLimit(10); // 限制最多10个结果
在数据较多的情况下，分页显示数据是比较合理的解决办法，setKip方法可以做到跳过首次查询的多少条数据来实现分页的功能。

query.setSkip(10); // 忽略前10个
对应数据的排序，如数字或字符串，你可以使用升序或降序的方式来控制查询数据的结果顺序：

// 根据score字段升序显示数据
query.orderByAscending("score");

// 根据score字段降序显示数据
query.orderByDescending("score");
//各种不同的比较查询：
// 分数 < 50
query.whereLessThan("score", 50);

//分数 <= 50
query.whereLessThanOrEqualTo("score", 50);

//分数 > 50
query.whereGreaterThan("score", 50);

//分数 >= 50
query.whereGreaterThanOrEqualTo("score", 50);
如果你想查询匹配几个不同值的数据，如：要查询“steve”，“chard”，“vj”三个人的成绩时，你可以使用whereContainedIn（类似SQL中的in查询）方法来实现。